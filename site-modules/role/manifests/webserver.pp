# This Role sets the profiles for the webserver
#
# @summary Configures webserver nodes
#
# @example
#   include role::webserver
class role::webserver {
  #This role would be made of all the profiles that need to be included to make a webserver work
  #All roles should include the base profile
  include profile::base
}
