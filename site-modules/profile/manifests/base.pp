# This class is the base configuration
#
# @summary Base profile for global configuration
#
# @example
#   include profile::base
class profile::base {
  #the base profile should include component modules that will be on all nodes
}
